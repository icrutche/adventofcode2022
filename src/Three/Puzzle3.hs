module Three.Puzzle3 (
    findDuplicateCharValue,
    findCommonVal,
    chunks
) where

import Data.Char (ord, isAsciiUpper)
import Data.List (foldl')
import qualified Data.Set as Set

{-|
    Given a string of even length, find the common characters between the first and second half,
    convert to acsii character codes, and then find the total sum

    Approach:
    1. Split by lines
    2. Split in half
    3. Create a set from each half
    4. Intersect the sets
    5. Convert to ascii
    6. sum
-}
findDuplicateCharValue :: String -> Int 
findDuplicateCharValue = toInt . findDuplicateChar

findDuplicateChar :: String -> Char
findDuplicateChar s = Set.elemAt 0 $ Set.intersection f l
    where (f,l) = halves s

halves :: String -> (Set.Set Char, Set.Set Char)
halves s = (Set.fromList f, Set.fromList l)
    where (f,l) = splitAt midpoint s
          midpoint = div (length s) 2
        
{-|
    ASCII values for characters (A-Z) -> (65,90) and (a-z) -> (97-118), so we have to have some function
    that maps those domains to the correct range

    If A-Z, subtract so that 65 is mapped to 27 (65 - x = 27, so 65-27 = 38)
    If a-z, subtract so that 97 is mapped to 1 (97 - x = 1, so 97-1 = 96)
-}
toInt :: Char -> Int
toInt c
    | isAsciiUpper c = ord c - (65 - 27)
    | otherwise = ord c - (97 - 1)

-- Part Two

{-|
    Find the common item between groups of three "bags" and it's value

    Approach:
    1. Take a list of strings to represent the bag groups
    2. Convert lists to sets
    3. Perform a three way intersection
    4. Convert common char to value

    So we will do some map operation that converts lists to sets, folds to an intersection, then extracts
    the first element which will be the intersection of all three sets
-}
findCommonVal :: [String] -> Int
findCommonVal ss = toInt $ Set.elemAt 0 intersection
    where intersection = foldl' Set.intersection f t
          (f:t) = map Set.fromList ss

chunks :: Int -> [String] -> [[String]]
chunks _ [] = []
chunks n ss = h : chunks n t
    where (h,t) = splitAt n ss