module Four.Puzzle4 (
    checkRange,
    eitherContains,
    overlapsWith
) where

import Data.Either (fromRight)
import Data.Text.Read (decimal)
import qualified Data.Text as T

{-|
    Check the ranges to see if they satisfy the given predicate

    This is common to both solutiuons, and allows us to swap what condition we are inspecting 
    the ranges for
-}
checkRange :: ((Int, Int) -> (Int, Int) -> Bool) -> T.Text -> Bool
checkRange p t = p a b
    where ranges = T.splitOn "," t
          a = toTuple (ranges !! 0)
          b = toTuple (ranges !! 1)

toTuple :: T.Text -> (Int, Int)
toTuple t = (toInt a, toInt b)
    where values = T.splitOn "-" t
          a = values !! 0
          b = values !! 1

toInt :: T.Text -> Int
toInt t = fst $ fromRight (0, "") $ decimal t

{-|
    Part 1 Predicate:

    Find if the first range contains all the numbers from the second range. Both ranges 
    are inclusive.

    Approach:

    Since the ranges are contiguous, we can just check the boundaries. One boundary contains the 
    other if the left most bounds is less than or equal to the other left most bound, AND if the
    right most bound is greater than or equal. 
-}
eitherContains :: (Int, Int) -> (Int, Int) -> Bool
eitherContains a b = contains a b || contains b a

contains :: (Int, Int) -> (Int, Int) -> Bool
contains (a, b) (c, d) = (a <= c) && (b >= d)

{-|
    Part 2 Predicate:

    Find if the ranges overlap at all

    Approach:

    Two ranges "overlap" if either of the boundaries of one is within the boundary of the other. So
    we will need to check if the start or end of a boundary is contained within the start and end 
    of the other, or if the boundary completely subsumes the other
-}
overlapsWith :: (Int, Int) -> (Int, Int) -> Bool
overlapsWith (a, b) (c, d) = (a >= c && a <= d) || (b >= c && b <= d) || (a <= c && b >= d)