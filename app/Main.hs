module Main (main) where

import qualified Data.Text as T
import Data.Foldable (foldl')

import Three.Puzzle3
import Four.Puzzle4

main :: IO Int
main = do
    s <- readFile "input/day4/input.txt"
    return $ day4Part2 s

day4Part1 :: String -> Int
day4Part1 s = foldl' countTrue 0 results
    where results = map (checkRange eitherContains . T.pack) (lines s) 
          countTrue x b = if b then x + 1 else x

-- notice that this is identical to part 1, but for the "predicate" passed to checkRange
day4Part2 :: String -> Int
day4Part2 s = foldl' countTrue 0 results
    where results = map (checkRange overlapsWith . T.pack) (lines s)
          countTrue x b = if b then x + 1 else x
         

day3Part2 :: String -> Int
day3Part2 s = sum $ map findCommonVal (chunks 3 (lines s))